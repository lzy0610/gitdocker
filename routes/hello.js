const express = require('express');
const router = express.Router();

const helloController = require('../controllers/hello-controller.js');

/* GET home page. */
router.get('/', function(req, res){
	res.send(helloController.helloWorld());
});

module.exports = router;
